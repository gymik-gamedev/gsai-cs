using System;
using Godot;
using Godot.Collections;

/**
 * @brief Base container type that stores data to find neighbors of an agent.
 */
public class GSAIProximity : Reference
{
	/**
	 * @brief The owning agent whose neighbors are found in the group.
	 */
	public GSAISteeringAgent Agent { get; set; }
	
	/**
	 * @brief Agents that could be potential neighbors of the owning agent.
	 */
	public Array<GSAISteeringAgent> Agents { get; set; }
	
	public GSAIProximity(
		GSAISteeringAgent agent,
		Array<GSAISteeringAgent> agents)
	{
		Agent = agent;
		Agents = agents;
	}
	
	/**
	 * @return The number of neighbors in proximity.
	 */
	public virtual int FindNeighbors(Func<GSAISteeringAgent, bool> callback)
	{
		return 0;
	}
}

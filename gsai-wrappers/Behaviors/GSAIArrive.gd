class_name GSAIArrive
extends GSAISteeringBehavior

var target: GSAIAgentLocation setget _set_target, _get_target
var arrival_tolerance: float setget _set_arrival_tolerance, \
		_get_arrival_tolerance
var deceleration_radius: float setget _set_deceleration_radius, \
		_get_deceleration_radius
var time_to_reach: float setget _set_time_to_reach, _get_time_to_reach

func _init(_agent: GSAISteeringAgent = null, \
		_target: GSAIAgentLocation = null) -> void:
	if _agent == null or _target == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIArrive").new(_agent._cs, _target._cs)

func _set_target(new: GSAIAgentLocation) -> void:
	_cs.Target = new._cs

func _get_target() -> GSAIAgentLocation:
	return _get_wrapper_from_cs(_cs.Target) as GSAIAgentLocation

func _set_arrival_tolerance(new: float) -> void:
	_cs.ArrivalTolerance = new

func _get_arrival_tolerance() -> float:
	return _cs.ArrivalTolerance

func _set_deceleration_radius(new: float) -> void:
	_cs.DecelerationRadius = new

func _get_deceleration_radius() -> float:
	return _cs.DecelerationRadius

func _set_time_to_reach(new: float) -> void:
	_cs.TimeToReach = new

func _get_time_to_reach() -> float:
	return _cs.TimeToReach

using System;
using Godot;

/**
 * @brief Steers the agent to avoid obstacles in its path.
 * @note Approximates obstacles as spheres.
 */
public class GSAIAvoidCollisions : GSAIGroupBehavior
{
	private GSAISteeringAgent _firstNeighbor = null;
	private float _shortestTime = 0.0f;
	private float _firstMinimumSeparation = 0.0f;
	private float _firstDistance = 0.0f;
	private Vector3 _firstRelativePosition = Vector3.Zero;
	private Vector3 _firstRelativeVelocity = Vector3.Zero;
	
	public GSAIAvoidCollisions(
		GSAISteeringAgent agent,
		GSAIProximity proximity
	) : base(agent, proximity)
	{
		return;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		_shortestTime = Mathf.Inf;
		_firstNeighbor = null;
		_firstMinimumSeparation = 0.0f;
		_firstDistance = 0.0f;
		
		int neighborCount = Proximity.FindNeighbors(_callback);
		
		if (neighborCount == 0 || _firstNeighbor == null)
		{
			acceleration.SetZero();
		}
		else
		{
			if (_firstMinimumSeparation <= 0.0f || _firstDistance
			< Agent.BoundingRadius + _firstNeighbor.BoundingRadius)
			{
				acceleration.Linear = _firstNeighbor.Position - Agent.Position;
			}
			else
			{
				acceleration.Linear = _firstRelativePosition
					+ _firstRelativeVelocity * _shortestTime;
			}
		}
		
		acceleration.Linear = 
			acceleration.Linear.Normalized() * -Agent.LinearAccelerationMax;
		acceleration.Angular = 0.0f;
	}
	
	/**
	 * @brief Keeps track of every neighbor, but only keeps the one the owning
	 *        agent is most likely to collide with.
	 */
	protected override bool reportNeighbor(GSAISteeringAgent neighbor)
	{
		Vector3 relativePosition = neighbor.Position - Agent.Position;
		Vector3 relativeVelocity =
			neighbor.LinearVelocity - Agent.LinearVelocity;
		float relativeSpeedSquared = relativeVelocity.LengthSquared();
		
		if (relativeSpeedSquared == 0.0f)
		{
			return false;
		}
		
		float timeToCollision =
			-relativePosition.Dot(relativeVelocity) / relativeSpeedSquared;
		
		if (timeToCollision <= 0.0f || timeToCollision >= _shortestTime)
		{
			return false;
		}
		
		float distance = relativePosition.Length();
		float minimumSeparation =
			distance - relativeVelocity.Length() * timeToCollision;
		
		if (minimumSeparation > Agent.BoundingRadius + neighbor.BoundingRadius)
		{
			return false;
		}
		
		_shortestTime = timeToCollision;
		_firstNeighbor = neighbor;
		_firstMinimumSeparation = minimumSeparation;
		_firstDistance = distance;
		_firstRelativePosition = relativePosition;
		_firstRelativeVelocity = relativeVelocity;
		
		return true;
	}
}

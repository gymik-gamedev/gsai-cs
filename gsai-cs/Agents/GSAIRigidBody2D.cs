using System;
using Godot;

/**
 * @brief Specialized steering agent based on RigidBody2D that auto-updates
 *        itself every frame.
 */
public class GSAIRigidBody2DAgent : GSAISpecializedAgent
{
	/**
	 * @brief The RigidBody2D to keep track of.
	 */
	public RigidBody2D Body { get; set; }
	
	/**
	 * @warning Body is set only after the body gets inside tree!
	 */
	public GSAIRigidBody2DAgent(RigidBody2D body)
	{
		if (!body.IsInsideTree())
		{
			Godot.Collections.Array args = new Godot.Collections.Array();
			args.Add(body);
			body.Connect("ready", this, nameof(onBodyReady), args);
		}
		else
		{
			onBodyReady(body);
		}
	}
	
	/**
	 * @brief Updates position, orientation, and velocities.
	 * @note Triggered automatically each frame by Godot.
	 */
	public void OnSceneTreePhysicsFrame()
	{
		if (Body == null)
		{
			return;
		}
		
		Position = GSAIUtils.ToVector3(Body.GlobalPosition);
		Orientation = Body.Rotation;
		
		if (!CalculateVelocities)
		{
			return;
		}
		else if (_appliedSteering)
		{
			_appliedSteering = false;
			return;
		}
		
		LinearVelocity = GSAIUtils.ToVector3(Body.LinearVelocity);
		AngularVelocity = Body.AngularVelocity;
	}
	
	public override void ApplySteering(
		GSAITargetAcceleration acceleration,
		float delta)
	{
		_appliedSteering = true;
		Body.ApplyCentralImpulse(GSAIUtils.ToVector2(acceleration.Linear));
		Body.ApplyTorqueImpulse(acceleration.Angular);
		
		if (CalculateVelocities)
		{
			LinearVelocity = GSAIUtils.ToVector3(Body.LinearVelocity);
			AngularVelocity = Body.AngularVelocity;
		}
	}
	
	private void onBodyReady(RigidBody2D body)
	{
		Body = body;
		Body.GetTree().Connect(
			"physics_frame",
			this,
			nameof(OnSceneTreePhysicsFrame));
	}
}

using System;
using Godot;

/**
 * @brief Calculates angular acceleration to rotate a target to face its target,
 *        attempting to arrive with zero remaining angular velocity.
 */
public class GSAIFace : GSAIMatchOrientation
{
	public GSAIFace(
		GSAISteeringAgent agent,
		GSAIAgentLocation target,
		bool useZ = false
	) : base(agent, target, useZ)
	{
		return;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		face(acceleration, Target.Position);
	}
	
	private void face(
		GSAITargetAcceleration acceleration,
		Vector3 targetPosition)
	{
		Vector3 toTarget = targetPosition - Agent.Position;
		float distanceSquared = toTarget.LengthSquared();
		
		if (distanceSquared < Agent.ZeroLinearSpeedThreshold)
		{
			acceleration.SetZero();
			return;
		}
		
		float orientation = UseZ ?
			GSAIUtils.Vector3ToAngle(toTarget) :
			GSAIUtils.Vector2ToAngle(GSAIUtils.ToVector2(toTarget));
		
		matchOrientation(acceleration, orientation);
	}
}

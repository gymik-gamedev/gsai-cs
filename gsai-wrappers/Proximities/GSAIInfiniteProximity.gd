class_name GSAIInfiniteProximity
extends GSAIProximity

func _init(_agent: GSAISteeringAgent = null, _agents: Array = []) -> void:
	if _agent == null:
		return
	
	var agents_cs: Array = []
	for agent in _agents:
		agents_cs.append(agent._cs)
	
	_cs = _get_cs("Proximities/GSAIInfiniteProximity").new(_agent._cs, \
			agents_cs)

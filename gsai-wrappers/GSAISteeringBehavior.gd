class_name GSAISteeringBehavior
extends GSAIWrapper

var is_enabled: bool setget _set_is_enabled, _get_is_enabled
var agent: GSAISteeringAgent setget _set_agent, _get_agent

func _init(_agent: GSAISteeringAgent = null) -> void:
	if _agent == null:
		return
	
	_cs = _get_cs("GSAISteeringBehavior").new(_agent._cs)

func calculate_steering(acceleration: GSAITargetAcceleration) -> void:
	_cs.CalculateSteering(acceleration._cs)

func _set_is_enabled(new: bool) -> void:
	_cs.IsEnabled = new

func _get_is_enabled() -> bool:
	return _cs.IsEnabled

func _set_agent(new: GSAISteeringAgent) -> void:
	_cs.Agent = new._cs

func _get_agent() -> GSAISteeringAgent:
	return _get_wrapper_from_cs(_cs.Agent) as GSAISteeringAgent

using System;
using Godot;

/**
 * @brief Specialized steering agent based on RigidBody that auto-updates
 *        itself every frame.
 */
public class GSAIRigidBody3DAgent : GSAISpecializedAgent
{
	/**
	 * @brief The RigidBody to keep track of.
	 */
	public RigidBody Body { get; set; }
	
	/**
	 * @warning Body is set only after the body gets inside tree!
	 */
	public GSAIRigidBody3DAgent(RigidBody body)
	{
		if (!body.IsInsideTree())
		{
			Godot.Collections.Array args = new Godot.Collections.Array();
			args.Add(body);
			body.Connect("ready", this, nameof(onBodyReady));
		}
		else
		{
			onBodyReady(body);
		}
	}
	
	/**
	 * @brief Updates position, orientation, and velocities.
	 * @note Triggered automatically each frame by Godot.
	 */
	public void OnSceneTreePhysicsFrame()
	{
		if (Body == null)
		{
			return;
		}
		
		Position = Body.Transform.origin;
		Orientation = Body.Rotation.y;
		
		if (!CalculateVelocities)
		{
			return;
		}
		else if (_appliedSteering)
		{
			_appliedSteering = false;
			return;
		}
		
		LinearVelocity = Body.LinearVelocity;
		AngularVelocity = Body.AngularVelocity.y;
	}
	
	public override void ApplySteering(
		GSAITargetAcceleration acceleration,
		float delta)
	{
		_appliedSteering = true;
		Body.ApplyCentralImpulse(acceleration.Linear);
		Body.ApplyTorqueImpulse(Vector3.Up * acceleration.Angular);
		
		if (CalculateVelocities)
		{
			LinearVelocity = Body.LinearVelocity;
			AngularVelocity = Body.AngularVelocity.y;
		}
	}
	
	private void onBodyReady(RigidBody body)
	{
		Body = body;
		Body.GetTree().Connect(
			"physics_frame",
			this,
			nameof(OnSceneTreePhysicsFrame));
	}
}

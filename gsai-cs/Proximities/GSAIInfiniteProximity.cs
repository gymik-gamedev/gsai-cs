using System;
using Godot;
using Godot.Collections;

/**
 * @brief Determines any agent in the list as being neighbors with the owning
 *        agent, regardless of distance.
 */
public class GSAIInfiniteProximity : GSAIProximity
{
	public GSAIInfiniteProximity(
		GSAISteeringAgent agent,
		Array<GSAISteeringAgent> agents
	) : base(agent, agents)
	{
		return;
	}
	
	public override int FindNeighbors(Func<GSAISteeringAgent, bool> callback)
	{
		int neighborCount = 0;
		
		foreach (GSAISteeringAgent currentAgent in Agents)
		{
			if (currentAgent != Agent && callback(currentAgent))
			{
				neighborCount++;
			}
		}
		
		return neighborCount;
	}
}

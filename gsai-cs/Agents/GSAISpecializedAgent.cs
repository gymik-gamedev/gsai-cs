using System;
using Godot;

/**
 * @brief Base class for specialized steering agents that update themselves.
 */
public class GSAISpecializedAgent : GSAISteeringAgent
{
	protected float _lastOrientation;
	protected bool _appliedSteering = false;
	
	/**
	 * @brief If false, velocities aren't auto-updated.
	 */
	public bool CalculateVelocities { get; set; } = true;
	
	/**
	 * @brief Interpolates current linear velocity towards zero by the
	 *        `LinearDragPercentage` value if true.
	 * @note Does not apply to rigid bodies.
	 */
	public bool ApplyLinearDrag { get; set; } = true;
	
	/**
	 * @brief Interpolates current angular velocity towards zero by the
	 *        `AngularDragPercentage` value if true.
	 */
	public bool ApplyAngularDrag { get; set; } = true;
	public float LinearDragPercentage { get; set; } = 0.0f;
	public float AngularDragPercentage { get; set; } = 0.0f;
	
	/**
	 * @brief Moves the agent's body by target `acceleration`.
	 */
	public virtual void ApplySteering(
		GSAITargetAcceleration acceleration,
		float delta)
	{
		return;
	}
}

using System;
using Godot;

/**
 * @brief Adds velocity, speed, and size data to `GSAIAgentLocation`.
 * @note It is the character's responsibility to keep this information up to
 *       date for the steering toolkit to work correctly.
 */
public class GSAISteeringAgent : GSAIAgentLocation
{
	/**
	 * @brief The amount of velocity to be considered effectively not moving.
	 */
	public float ZeroLinearSpeedThreshold { get; set; } = 0.01f;
	
	/**
	 * @brief The maximum speed at which the agent can move.
	 */
	public float LinearSpeedMax { get; set; } = 0.0f;
	
	/**
	 * @brief The maximum amount of acceleration that any behavior can apply to
	 *        the agent.
	 */
	public float LinearAccelerationMax { get; set; } = 0.0f;
	
	/**
	 * @brief The maximum amount of angular speed at which the agent can rotate.
	 */
	public float AngularSpeedMax { get; set; } = 0.0f;
	
	/**
	 * @brief The maximum amount of angular acceleration that any behavior can
	 *        apply to an agent.
	 */
	public float AngularAccelerationMax { get; set; } = 0.0f;
	
	/**
	 * @brief Current velocity of the agent.
	 */
	public Vector3 LinearVelocity { get; set; } = Vector3.Zero;
	
	/**
	 * @brief Current angular velocity of the agent.
	 */
	public float AngularVelocity { get; set; } = 0.0f;
	
	/**
	 * @brief Radius of a sphere that approximates the agent's size in space.
	 */
	public float BoundingRadius { get; set; } = 0.0f;
	
	/**
	 * @brief Used internally by group behaviors and proximites to mark the
	 *        agent as already considered.
	 */
	public bool IsTagged { get; set; } = false;
}

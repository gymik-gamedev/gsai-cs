using System;
using Godot;

/**
 * @brief Calculates angular acceleration to match the agent's orientation to
 *        that of its target, attempts to arrive with zero remaining velocity.
 */
public class GSAIMatchOrientation : GSAISteeringBehavior
{
	public GSAIAgentLocation Target { get; set; }
	
	/**
	 * @brief Amount of distance in radians for the behavior to consider itself
	 *        matching the target's rotation.
	 */
	public float AlignmentTolerance { get; set; }
	public float DecelerationRadius { get; set; }
	
	/**
	 * @brief Time in seconds needed to change acceleration.
	 */
	public float TimeToReach { get; set; } = 0.1f;
	
	/**
	 * @brief Whether to use the X and Z components instead of X and Y for
	 *        determining angles.
	 * @note X and Z should be used in 3D.
	 */
	public bool UseZ { get; set; }
	
	public GSAIMatchOrientation(
		GSAISteeringAgent agent,
		GSAIAgentLocation target,
		bool useZ = false
	) : base(agent)
	{
		UseZ = useZ;
		Target = target;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		matchOrientation(acceleration, Target.Orientation);
	}
	
	protected void matchOrientation(
		GSAITargetAcceleration acceleration,
		float desiredOrientation)
	{
		float rotation = Mathf.Wrap(
			desiredOrientation - Agent.Orientation, -Mathf.Pi, Mathf.Pi);
		float rotationSize = Mathf.Abs(rotation);
		
		if (rotationSize <= AlignmentTolerance)
		{
			acceleration.SetZero();
			return;
		}
		
		float desiredRotation = Agent.AngularSpeedMax;
		
		if (rotationSize <= DecelerationRadius)
		{
			desiredRotation *= rotationSize / DecelerationRadius;
		}
		
		desiredRotation *= rotation / rotationSize;
		acceleration.Angular =
			(desiredRotation - Agent.AngularVelocity) / TimeToReach;
		
		float limitedAcceleration = Mathf.Abs(acceleration.Angular);
		if (limitedAcceleration > Agent.AngularAccelerationMax)
		{
			acceleration.Angular *=
				Agent.AngularAccelerationMax / limitedAcceleration;
		}
		
		acceleration.Linear = Vector3.Zero;
	}
}

using System;
using Godot;

/**
 * @brief Calculates an acceleration that repels the agent from its neighbors
 *        in the given proximity.
 *
 * The acceleration is an average based on all neighbors, multiplied by strength
 * decreasing by the inverse square law in relation to distance, and it
 * accumulates.
 */
public class GSAISeparation : GSAIGroupBehavior
{
	private GSAITargetAcceleration _acceleration = null;
	
	/**
	 * @brief Coefficient representing how fast the separation strength decays
	 *        with distance,
	 */
	public float DecayCoefficient { get; set; } = 1.0f;
	
	public GSAISeparation(
		GSAISteeringAgent agent, GSAIProximity proximity
	) : base(agent, proximity)
	{
		return;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		acceleration.SetZero();
		_acceleration = acceleration;
		Proximity.FindNeighbors(_callback);
	}
	
	/**
	 * @brief Determines the amount of acceleration that `neighbor` imposes
	 *        based on its distance from the owning agent.
	 */
	protected override bool reportNeighbor(GSAISteeringAgent neighbor)
	{
		Vector3 toAgent = Agent.Position - neighbor.Position;
		float strength = DecayCoefficient / toAgent.LengthSquared();
		
		if (strength > Agent.LinearAccelerationMax)
		{
			strength = Agent.LinearAccelerationMax;
		}
		
		_acceleration.Linear += toAgent * (strength / toAgent.Length());
		return true;
	}
}

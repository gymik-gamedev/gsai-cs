using System;
using Godot;

/**
 * @brief Calculates angular acceleration to match the agent's orientation to
 *        its direction of travel.
 */
public class GSAILookWhereYouGo : GSAIMatchOrientation
{
	public GSAILookWhereYouGo(
		GSAISteeringAgent agent,
		bool useZ = false
	) : base(agent, null, useZ)
	{
		return;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		if (Agent.LinearVelocity.LengthSquared()
			< Agent.ZeroLinearSpeedThreshold)
		{
			acceleration.SetZero();
			return;
		}
		
		float orientation = UseZ ? 
			GSAIUtils.Vector3ToAngle(Agent.LinearVelocity) :
			GSAIUtils.Vector2ToAngle(GSAIUtils.ToVector2(Agent.LinearVelocity));
		
		matchOrientation(acceleration, orientation);
	}
}

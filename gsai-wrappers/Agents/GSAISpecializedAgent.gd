class_name GSAISpecializedAgent
extends GSAISteeringAgent

var calculate_velocities: bool setget _set_calculate_velocities, \
		_get_calculate_velocities
var apply_linear_drag: bool setget _set_apply_linear_drag, \
		_get_apply_linear_drag
var apply_angular_drag: bool setget _set_apply_angular_drag, \
		_get_apply_angular_drag
var linear_drag_percentage: float setget _set_linear_drag_percentage, \
		_get_linear_drag_percentage
var angular_drag_percentage: float setget _set_angular_drag_percentage, \
		_get_angular_drag_percentage

func _init() -> void:
	_cs = _get_cs("Agents/GSAISpecializedAgent").new()

# This is actually a public method, blame the original GSAI author for confusion
func _apply_steering(_accel: GSAITargetAcceleration, delta: float) -> void:
	_cs.ApplySteering(_accel._cs, delta)

func _set_calculate_velocities(new: bool) -> void:
	_cs.CalculateVelocities = new

func _get_calculate_velocities() -> bool:
	return _cs.CalculateVelocities

func _set_apply_linear_drag(new: bool) -> void:
	_cs.ApplyLinearDrag = new

func _get_apply_linear_drag() -> bool:
	return _cs.ApplyLinearDrag

func _set_apply_angular_drag(new: bool) -> void:
	_cs.ApplyAngularDrag = new

func _get_apply_angular_drag() -> bool:
	return _cs.ApplyAngularDrag

func _set_linear_drag_percentage(new: float) -> void:
	_cs.LinearDragPercentage = new

func _get_linear_drag_percentage() -> float:
	return _cs.LinearDragPercentage

func _set_angular_drag_percentage(new: float) -> void:
	_cs.AngularDragPercentage = new

func _get_angular_drag_percentage() -> float:
	return _cs.AngularDragPercentage

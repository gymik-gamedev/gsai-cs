class_name GSAISeek
extends GSAISteeringBehavior

var target: GSAIAgentLocation setget _set_target, _get_target

func _init(_agent: GSAISteeringAgent = null, _target: GSAIAgentLocation \
		= null) -> void:
	if _agent == null or _target == null:
		return
	
	_cs = _get_cs("Behaviors/GSAISeek").new(_agent._cs, _target._cs)

func _set_target(new: GSAIAgentLocation) -> void:
	_cs.Target = new._cs

func _get_target() -> GSAIAgentLocation:
	return _get_wrapper_from_cs(_cs.Target) as GSAIAgentLocation

class_name GSAIRadiusProximity
extends GSAIProximity

var radius: float setget _set_radius, _get_radius

func _init(_agent: GSAISteeringAgent = null, _agents: Array = [], \
		_radius: float = 0.0) -> void:
	if _agent == null:
		return
	
	var agents_cs: Array = []
	for agent in _agents:
		agents_cs.append(agent._cs)
	
	_cs = _get_cs("Proximities/GSAIRadiusProximity").new(_agent._cs, \
			agents_cs, _radius)

func _set_radius(new: float) -> void:
	_cs.Radius = new

func _get_radius() -> float:
	return _cs.Radius

using System;
using Godot;

/**
 * @brief A desired amount of acceleration requested by the steering system.
 */
public class GSAITargetAcceleration : Godot.Object
{
	public Vector3 Linear { get; set; } = Vector3.Zero;
	public float Angular { get; set; } = 0.0f;
	
	/**
	 * @brief Sets the linear and angular components to 0.
	 */
	public void SetZero()
	{
		Linear = Vector3.Zero;
		Angular = 0.0f;
	}
	
	/**
	 * @brief Adds `accel`'s components, multiplied by `scalar`, to this one.
	 */
	public void AddScaledAccel(GSAITargetAcceleration accel, float scalar)
	{
		Linear += accel.Linear * scalar;
		Angular += accel.Angular * scalar;
	}
	
	/**
	 * @return The squared magnitude of the linear and angular components.
	 */
	public float GetMagnitudeSquared()
	{
		return Linear.LengthSquared() + Angular * Angular;
	}
	
	/**
	 * @return The magnitude of the linear and angular components.
	 */
	public float GetMagnitude()
	{
		return Mathf.Sqrt(GetMagnitudeSquared());
	}
}

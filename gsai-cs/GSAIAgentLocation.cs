using System;
using Godot;

/**
 * @brief Represents an agent with only a location and an orientation.
 */
public class GSAIAgentLocation : Godot.Object
{
	public Vector3 Position { get; set; } = Vector3.Zero;
	public float Orientation { get; set; } = 0.0f;
}

class_name GSAIMatchOrientation
extends GSAISteeringBehavior

var target: GSAIAgentLocation setget _set_target, _get_target
var alignment_tolerance: float setget _set_alignment_tolerance, \
		_get_alignment_tolerance
var deceleration_radius: float setget _set_deceleration_radius, \
		_get_deceleration_radius
var time_to_reach: float setget _set_time_to_reach, _get_time_to_reach
var use_z: bool setget _set_use_z, _get_use_z

func _init(_agent: GSAISteeringAgent = null, _target: GSAIAgentLocation \
		= null, _use_z: bool = false) -> void:
	if _agent == null or _target == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIMatchOrientation").new(_agent._cs, \
			_target._cs, _use_z)

func _set_target(new: GSAIAgentLocation) -> void:
	_cs.Target = new._cs

func _get_target() -> GSAIAgentLocation:
	return _get_wrapper_from_cs(_cs.Target) as GSAIAgentLocation

func _set_alignment_tolerance(new: float) -> void:
	_cs.AlignmentTolerance = new

func _get_alignment_tolerance() -> float:
	return _cs.AlignmentTolerance

func _set_deceleration_radius(new: float) -> void:
	_cs.DecelerationRadius = new

func _get_deceleration_radius() -> float:
	return _cs.DecelerationRadius

func _set_time_to_reach(new: float) -> void:
	_cs.TimeToReach = new

func _get_time_to_reach() -> float:
	return _cs.TimeToReach

func _set_use_z(new: bool) -> void:
	_cs.UseZ = new

func _get_use_z() -> bool:
	return _cs.UseZ

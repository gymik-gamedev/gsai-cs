using System;
using Godot;

/**
 * @brief Calculates an acceleration moving the agent towards the center of mass
 *        of the given proximity.
 */
public class GSAICohesion : GSAIGroupBehavior
{
	private Vector3 _centerOfMass = Vector3.Zero;
	
	public GSAICohesion(
		GSAISteeringAgent agent,
		GSAIProximity proximity
	) : base(agent, proximity)
	{
		return;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		acceleration.SetZero();
		_centerOfMass = Vector3.Zero;
		int neighborCount = Proximity.FindNeighbors(_callback);
		
		if (neighborCount > 0)
		{
			_centerOfMass *= 1.0f / neighborCount;
			acceleration.Linear = Agent.Position.DirectionTo(_centerOfMass)
				* Agent.LinearAccelerationMax;
		}
	}
	
	/**
	 * @brief Adds `neighbor`'s position to the center of mass of the group.
	 */
	protected override bool reportNeighbor(GSAISteeringAgent neighbor)
	{
		_centerOfMass += neighbor.Position;
		return true;
	}
}

using System;
using Godot;

public class GSAICSharpContext : Godot.Object
{
	public static String GetClassName(object obj)
	{
		return obj.GetType().Name;
	}
}

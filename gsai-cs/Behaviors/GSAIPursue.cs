using System;
using Godot;

/**
 * @brief Calculates an acceleration to try make the agent intercept target.
 */
public class GSAIPursue : GSAISteeringBehavior
{
	public GSAISteeringAgent Target { get; set; }
	public float PredictTimeMax { get; set; }
	
	public GSAIPursue(
		GSAISteeringAgent agent,
		GSAISteeringAgent target,
		float predictTimeMax = 1.0f
	) : base(agent)
	{
		Target = target;
		PredictTimeMax = predictTimeMax;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		float distanceSquared =
			(Target.Position - Agent.Position).LengthSquared();
		
		float speedSquared = Agent.LinearVelocity.LengthSquared();
		float predictTime = PredictTimeMax;
		
		if (speedSquared > 0.0f)
		{
			float predictTimeSquared = distanceSquared / speedSquared;
			
			if (predictTimeSquared < PredictTimeMax * PredictTimeMax)
			{
				predictTime = Mathf.Sqrt(predictTimeSquared);
			}
		}
		
		acceleration.Linear = (((
				Target.Position + Target.LinearVelocity * predictTime
			) - Agent.Position).Normalized()) * getModifiedAcceleration();
		acceleration.Angular = 0.0f;
	}
	
	protected virtual float getModifiedAcceleration()
	{
		return Agent.LinearAccelerationMax;
	}
}

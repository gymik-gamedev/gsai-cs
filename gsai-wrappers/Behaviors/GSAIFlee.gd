class_name GSAIFlee
extends GSAISeek

func _init(_agent: GSAISteeringAgent = null, _target: GSAIAgentLocation \
		= null) -> void:
	if _agent == null or _target == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIFlee").new(_agent._cs, _target._cs)

using System;
using Godot;
using Godot.Collections;

/**
 * @brief Blends multiple steering behaviors into one and returns a weighted
 *        average acceleration based on their individual values.
 * @note The behaviors are internally stored as Godot Dictionaries of the form
 *       { "behavior": GSAISteeringBehavior, "weight": float }. A better type
 *       for this data couldn't be chosen as non-Godot-native types crash Godot.
 */
public class GSAIBlend : GSAISteeringBehavior
{
	private Array<Dictionary> _behaviors = new Array<Dictionary>();
	private GSAITargetAcceleration _accel = new GSAITargetAcceleration();
	
	public GSAIBlend(GSAISteeringAgent agent) : base(agent)
	{
		return;
	}
	
	/**
	 * @brief Appends a behavior to the internal array along with its `weight`.
	 */
	public void Add(GSAISteeringBehavior behavior, float weight)
	{
		behavior.Agent = Agent;
		_behaviors.Add(
			new Dictionary(){ { "behavior", behavior }, { "weight", weight } }
		);
	}
	
	/**
	 * @return The behavior at specified index, or empty Dictionary if none was
	 *         found.
	 */
	public Dictionary GetBehaviorAt(int index)
	{
		if (index < 0 || index >= _behaviors.Count)
		{
			GD.PushError(
				String.Format("Tried to get index {} in array of size {}", 
				index, _behaviors.Count)
			);
			return new Dictionary();
		}
		
		return _behaviors[index];
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration blendedAccel)
	{
		blendedAccel.SetZero();
		
		foreach (Dictionary bw in _behaviors)
		{
			((GSAISteeringBehavior) bw["behavior"]).CalculateSteering(_accel);
			blendedAccel.AddScaledAccel(_accel, (float) bw["weight"]);
		}
		
		blendedAccel.Linear = GSAIUtils.ClampedV3(
			blendedAccel.Linear, Agent.LinearAccelerationMax
		);
		
		blendedAccel.Angular = Mathf.Clamp(blendedAccel.Angular,
			-Agent.AngularAccelerationMax, Agent.AngularAccelerationMax
		);
	}
}

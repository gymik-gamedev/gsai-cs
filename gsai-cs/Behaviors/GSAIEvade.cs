using System;
using Godot;

/**
 * @brief Calculates acceleration to take an agent away from where the target is
 *        moving to.
 */
public class GSAIEvade : GSAIPursue
{
	public GSAIEvade(
		GSAISteeringAgent agent,
		GSAISteeringAgent target,
		float predictTimeMax = 1.0f
	) : base(agent, target, predictTimeMax)
	{
		return;
	}
	
	protected override float getModifiedAcceleration()
	{
		return -Agent.LinearAccelerationMax;
	}
}

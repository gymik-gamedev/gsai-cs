class_name GSAIUtils

static func clampedv3(vector: Vector3, limit: float) -> Vector3:
	var length_squared := vector.length_squared()
	var limit_squared := limit * limit
	if length_squared > limit_squared:
		vector *= sqrt(limit_squared / length_squared)
	return vector

static func vector3_to_angle(vector: Vector3) -> float:
	return atan2(vector.x, vector.z)

static func vector2_to_angle(vector: Vector2) -> float:
	return atan2(vector.x, -vector.y)

static func angle_to_vector2(angle: float) -> Vector2:
	return Vector2(sin(-angle), cos(angle))

static func to_vector2(vector: Vector3) -> Vector2:
	return Vector2(vector.x, vector.y)

static func to_vector3(vector: Vector2) -> Vector3:
	return Vector3(vector.x, vector.y, 0)

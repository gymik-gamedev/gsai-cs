class_name GSAIRigidBody2DAgent
extends GSAISpecializedAgent

var body: RigidBody2D setget _set_body, _get_body

func _init(_body: RigidBody2D = null) -> void:
	if _body == null:
		return
	
	_cs = _get_cs("Agents/GSAIRigidBody2DAgent").new(_body)

func _set_body(new: RigidBody2D) -> void:
	_cs.Body = new

func _get_body() -> RigidBody2D:
	return _cs.Body

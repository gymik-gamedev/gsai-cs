class_name GSAIAgentLocation
extends GSAIWrapper

var position: Vector3 setget _set_position, _get_position
var orientation: float setget _set_orientation, _get_orientation

func _init() -> void:
	_cs = _get_cs("GSAIAgentLocation").new()

func _set_position(new: Vector3) -> void:
	_cs.Position = new

func _get_position() -> Vector3:
	return _cs.Position

func _set_orientation(new: float) -> void:
	_cs.Orientation = new

func _get_orientation() -> float:
	return _cs.Orientation

using System;
using Godot;

/**
 * @brief Calculates acceleration to take an agent to its target's location,
 *        attempting to arrive with zero remaining velocity.
 */
public class GSAIArrive : GSAISteeringBehavior
{
	public GSAIAgentLocation Target { get; set; }
	
	/**
	 * @brief Distance from the agent to target to be considered as arrived.
	 */
	public float ArrivalTolerance { get; set; }
	public float DecelerationRadius { get; set; }
	
	/**
	 * @brief Time in seconds needed to change acceleration.
	 */
	public float TimeToReach { get; set; } = 0.1f;
	
	public GSAIArrive(
		GSAISteeringAgent agent,
		GSAIAgentLocation target
	) : base(agent)
	{
		Target = target;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		arrive(acceleration, Target.Position);
	}
	
	protected void arrive(
		GSAITargetAcceleration acceleration,
		Vector3 targetPosition)
	{
		Vector3 toTarget = targetPosition - Agent.Position;
		float distance = toTarget.Length();
		
		if (distance <= ArrivalTolerance)
		{
			acceleration.SetZero();
			return;
		}
		
		float desiredSpeed = Agent.LinearSpeedMax;
		
		if (distance <= DecelerationRadius)
		{
			desiredSpeed *= distance / DecelerationRadius;
		}
		
		Vector3 desiredVelocity = (
			toTarget * desiredSpeed / distance - Agent.LinearVelocity
		) * 1.0f / TimeToReach;
		
		acceleration.Linear = GSAIUtils.ClampedV3(
			desiredVelocity, Agent.LinearAccelerationMax
		);
		acceleration.Angular = 0.0f;
	}
}

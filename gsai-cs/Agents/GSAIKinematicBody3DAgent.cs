using System;
using Godot;

/**
 * @brief Specialized steering agent based on KinematicBody that auto-updates
 *        itself every frame.
 */
public class GSAIKinematicBody3DAgent : GSAIKinematicBodyAgent
{
	private KinematicBody _body;
	private Vector3 _lastPosition;
	
	/**
	 * @brief The KinematicBody to keep track of.
	 */
	public KinematicBody Body
	{
		get { return _body; }
		set
		{
			_body = value;
			
			_lastPosition = value.Transform.origin;
			_lastOrientation = value.Rotation.y;
			
			Position = _lastPosition;
			Orientation = _lastOrientation;
		}
	}
	
	/**
	 * @warning Body and MovementType are set only after the body gets inside
	 *          tree!
	 */
	public GSAIKinematicBody3DAgent(
		KinematicBody body,
		MovementTypes movementType = MovementTypes.SLIDE)
	{
		if (!body.IsInsideTree())
		{
			Godot.Collections.Array args = new Godot.Collections.Array();
			args.Add(body);
			args.Add(movementType);
			body.Connect("ready", this, nameof(onBodyReady), args);
		}
		else
		{
			onBodyReady(body, movementType);
		}
	}
	
	/**
	 * @brief Updates position, orientation, and velocities.
	 * @note Triggered automatically each frame by Godot.
	 */
	public void OnSceneTreePhysicsFrame()
	{
		if (Body == null)
		{
			return;
		}
		
		Vector3 currentPosition = Body.Transform.origin;
		float currentOrientation = Body.Rotation.y;
		
		Position = currentPosition;
		Orientation = currentOrientation;
		
		if (!CalculateVelocities)
		{
			return;
		}
		else if (_appliedSteering)
		{
			_appliedSteering = false;
			return;
		}
		
		LinearVelocity = adjustLinearVelocity(currentPosition - _lastPosition);
		AngularVelocity = adjustAngularVelocity(
			_lastOrientation - currentOrientation
		);
		
		_lastPosition = currentPosition;
		_lastOrientation = currentOrientation;
	}
	
	protected override void applySlidingSteering(
		Vector3 acceleration,
		float delta)
	{
		if (Body == null)
		{
			return;
		}
		
		Vector3 velocity = adjustLinearVelocity(
			LinearVelocity + acceleration * delta
		);
		velocity = Body.MoveAndSlide(velocity);
		
		if (CalculateVelocities)
		{
			LinearVelocity = velocity;
		}
	}
	
	protected override void applyCollideSteering(
		Vector3 acceleration,
		float delta)
	{
		if (Body == null)
		{
			return;
		}
		
		Vector3 velocity = adjustLinearVelocity(
			LinearVelocity + acceleration * delta
		);
		Body.MoveAndCollide(velocity * delta);
		
		if (CalculateVelocities)
		{
			LinearVelocity = velocity;
		}
	}
	
	protected override void applyPositionSteering(
		Vector3 acceleration,
		float delta)
	{
		if (Body == null)
		{
			return;
		}
		
		Vector3 velocity = adjustLinearVelocity(
			LinearVelocity + acceleration * delta
		);
		Body.Transform = new Transform(
			Body.Transform.basis, Body.Transform.origin + velocity * delta
		);
		
		if (CalculateVelocities)
		{
			LinearVelocity = velocity;
		}
	}
	
	protected override void applyOrientationSteering(
		float acceleration,
		float delta)
	{
		if (Body == null)
		{
			return;
		}
		
		float velocity = adjustAngularVelocity(
			AngularVelocity + acceleration * delta
		);
		Body.Rotation = new Vector3(
			Body.Rotation.x, Body.Rotation.y + velocity * delta, Body.Rotation.z
		);
		
		if (CalculateVelocities)
		{
			AngularVelocity = velocity;
		}
	}
	
	private void onBodyReady(KinematicBody body, MovementTypes movementType)
	{
		Body = body;
		MovementType = movementType;
		
		Body.GetTree().Connect(
			"physics_frame",
			this,
			nameof(OnSceneTreePhysicsFrame));
	}
}

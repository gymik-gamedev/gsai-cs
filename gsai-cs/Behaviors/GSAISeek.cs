using System;
using Godot;

/**
 * @brief Calculates an acceleration to take an agent to the target's position.
 */
public class GSAISeek : GSAISteeringBehavior
{
	public GSAIAgentLocation Target { get; set; }
	
	public GSAISeek(
		GSAISteeringAgent agent,
		GSAIAgentLocation target
	) : base(agent)
	{
		Target = target;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		Vector3 dirToTarget = Agent.Position.DirectionTo(Target.Position);
		acceleration.Linear = dirToTarget * Agent.LinearAccelerationMax;
		acceleration.Angular = 0.0f;
	}
}

class_name GSAIFollowPath
extends GSAIArrive

var path: GSAIPath setget _set_path, _get_path
var path_offset: float setget _set_path_offset, _get_path_offset
var is_arrive_enabled: bool setget _set_is_arrive_enabled, \
		_get_is_arrive_enabled
var prediction_time: float setget _set_prediction_time, _get_prediction_time

func _init(_agent: GSAISteeringAgent = null, _path: GSAIPath = null, \
		_path_offset: float = 0.0, _prediction_time: float = 0.0) -> void:
	if _agent == null or _path == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIFollowPath").new(_agent._cs, _path._cs, \
			_path_offset, _prediction_time)

func _set_path(new: GSAIPath) -> void:
	_cs.Path = new._cs

func _get_path() -> GSAIPath:
	return _get_wrapper_from_cs(_cs.Path) as GSAIPath

func _set_path_offset(new: float) -> void:
	_cs.PathOffset = new

func _get_path_offset() -> float:
	return _cs.PathOffset

func _set_is_arrive_enabled(new: bool) -> void:
	_cs.IsArriveEnabled = new

func _get_is_arrive_enabled() -> bool:
	return _cs.IsArriveEnabled

func _set_prediction_time(new: float) -> void:
	_cs.PredictionTime = new

func _get_prediction_time() -> float:
	return _cs.PredictionTime

class_name GSAIRigidBody3DAgent
extends GSAISpecializedAgent

var body: RigidBody setget _set_body, _get_body

func _init(_body: RigidBody = null) -> void:
	if _body == null:
		return
	
	_cs = _get_cs("Agents/GSAIRigidBody3DAgent").new(_body)

func _set_body(new: RigidBody) -> void:
	_cs.Body = new

func _get_body() -> RigidBody:
	return _cs.Body

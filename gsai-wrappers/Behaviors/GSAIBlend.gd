class_name GSAIBlend
extends GSAISteeringBehavior

func _init(_agent: GSAISteeringAgent = null) -> void:
	if _agent == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIBlend").new(_agent._cs)

func add(behavior: GSAISteeringBehavior, weight: float) -> void:
	_cs.Add(behavior._cs, weight)

func get_behavior_at(index: int) -> Dictionary:
	var dict: Dictionary = _cs.GetBehaviorAt(index)
	dict["behavior"] = _get_wrapper_from_cs(dict["behavior"])
	return dict

class_name GSAIGroupBehavior
extends GSAISteeringBehavior

var proximity: GSAIProximity setget _set_proximity, _get_proximity

func _init(_agent: GSAISteeringBehavior = null, \
		_proximity: GSAIProximity = null) -> void:
	if _agent == null or proximity == null:
		return
	
	_cs = _get_cs("GSAIGroupBehavior").new(_agent._cs, _proximity._cs)

func _set_proximity(new: GSAIProximity) -> void:
	if new == null:
		return
	
	_cs.Proximity = new._cs

func _get_proximity() -> GSAIProximity:
	return _get_wrapper_from_cs(_cs.Proximity) as GSAIProximity

using System;
using Godot;

/**
 * @brief Math and vector utility functions.
 */
public class GSAIUtils : Godot.Object
{
	/**
	 * @return The `vector` with its length capped to `limit`.
	 */
	public static Vector3 ClampedV3(Vector3 vector, float limit)
	{
		float lengthSquared = vector.LengthSquared();
		float limitSquared = limit * limit;
		
		if (lengthSquared > limitSquared)
		{
			vector *= Mathf.Sqrt(limitSquared / lengthSquared);
		}
		
		return vector;
	}
	
	/**
	 * @return An angle in radians between the positive X axis and the `vector`.
	 * @note This assumes orientation for 3D agents that are upright and rotate
	 *       around the Y axis.
	 */
	public static float Vector3ToAngle(Vector3 vector)
	{
		return Mathf.Atan2(vector.x, vector.z);
	}
	
	/**
	 * @return An angle in radians between the positive X axis and the `vector`.
	 */
	public static float Vector2ToAngle(Vector2 vector)
	{
		return Mathf.Atan2(vector.x, -vector.y);
	}
	
	/**
	 * @return An angle in radians between the positive X axis and the `vector`.
	 * @note This assumes orientation for 2D agents or 3D agents that are
	 *       upright and rotate around the Y axis.
	 */
	public static Vector2 AngleToVector2(float angle)
	{
		return new Vector2(Mathf.Sin(-angle), Mathf.Cos(angle));
	}
	
	/**
	 * @return A Vector2 with `vector`'s x and y components.
	 */
	public static Vector2 ToVector2(Vector3 vector)
	{
		return new Vector2(vector.x, vector.y);
	}
	
	/**
	 * @return A Vector3 with `vector`'s x and y components and 0 in z.
	 */
	public static Vector3 ToVector3(Vector2 vector)
	{
		return new Vector3(vector.x, vector.y, 0.0f);
	}
}

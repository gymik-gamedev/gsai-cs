using System;
using Godot;
using Godot.Collections;

/**
 * @brief Determines any agent in the list that lies within the specified radius
 *        as being neighbors with the owning agent.
 */
public class GSAIRadiusProximity : GSAIProximity
{
	private long _lastFrame = 0;
	private SceneTree _sceneTree = null;
	
	public float Radius { get; set; } = 0.0f;
	
	public GSAIRadiusProximity(
		GSAISteeringAgent agent,
		Array<GSAISteeringAgent> agents,
		float radius
	) : base(agent, agents)
	{
		Radius = radius;
		_sceneTree = (SceneTree) Engine.GetMainLoop();
	}
	
	public override int FindNeighbors(Func<GSAISteeringAgent, bool> callback)
	{
		int neighborCount = 0;
		
		long currentFrame = _sceneTree != null ?
			_sceneTree.GetFrame() :
			-_lastFrame;
		
		if (currentFrame != _lastFrame)
		{
			_lastFrame = currentFrame;
			
			foreach (GSAISteeringAgent currentAgent in Agents)
			{
				if (currentAgent != Agent)
				{
					float distanceSquared =
						Agent.Position.DistanceSquaredTo(currentAgent.Position);
					
					float rangeTo = Radius + currentAgent.BoundingRadius;
					
					if (distanceSquared < rangeTo * rangeTo
						&& callback(currentAgent))
					{
						currentAgent.IsTagged = true;
						neighborCount++;
						continue;
					}
				}
				
				currentAgent.IsTagged = false;
			}
		}
		else
		{
			foreach (GSAISteeringAgent currentAgent in Agents)
			{
				if (currentAgent != Agent && currentAgent.IsTagged
					&& callback(currentAgent))
				{
					neighborCount++;
				}
			}
		}
		
		return neighborCount;
	}
}

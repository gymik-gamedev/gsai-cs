using System;
using Godot;
using Godot.Collections;

/**
 * @brief Returns the result of the first behavior with non-zero acceleration.
 */
public class GSAIPriority : GSAISteeringBehavior
{
	private Array<GSAISteeringBehavior> _behaviors
		= new Array<GSAISteeringBehavior>();
	
	/**
	 * @brief Index of the last behavior the container prioritized.
	 */
	public int LastSelectedIndex { get; set; }
	
	/**
	 * @brief If an acceleration is lower than this value, it's treated as zero.
	 */
	public float ZeroThreshold { get; set; }
	
	public GSAIPriority(
		GSAISteeringAgent agent,
		float zeroThreshold = 0.001F
	) : base(agent)
	{
		ZeroThreshold = zeroThreshold;
	}
	
	/**
	 * @brief Appends a behavior to the internal array.
	 */
	public void Add(GSAISteeringBehavior behavior)
	{
		_behaviors.Add(behavior);
	}
	
	/**
	 * @return The behavior at specified index, or null if none was found.
	 */
	public GSAISteeringBehavior GetBehaviorAt(int index)
	{
		if (index < 0 || index >= _behaviors.Count)
		{
			GD.PushError(
				String.Format("Tried to get index {} in array of size {}", 
				index, _behaviors.Count)
			);
			return null;
		}
		
		return _behaviors[index];
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		float thresholdSquared = ZeroThreshold * ZeroThreshold;
		
		LastSelectedIndex = -1;
		
		if (_behaviors.Count == 0.0)
		{
			acceleration.SetZero();
			return;
		}
		
		foreach (GSAISteeringBehavior behavior in _behaviors)
		{
			LastSelectedIndex++;
			behavior.CalculateSteering(acceleration);
			
			if (acceleration.GetMagnitudeSquared() > thresholdSquared)
			{
				return;
			}
		}
	}
}

class_name GSAILookWhereYouGo
extends GSAIMatchOrientation

func _init(_agent: GSAISteeringAgent = null, use_z: bool = false) -> void:
	if _agent == null:
		return
	
	_cs = _get_cs("Behaviors/GSAILookWhereYouGo").new(_agent._cs, use_z)

using System;
using Godot;
using Godot.Collections;

/**
 * @brief Represents a path made up of Vector3 waypoints, split into segments
 *        that path follow behaviors can use.
 */
public class GSAIPath : Reference
{
	private class GSAISegment : object
	{
		public Vector3 Begin { get; set; }
		public Vector3 End { get; set; }
		public float Length { get; set; }
		public float CumulativeLength { get; set; }
		
		public GSAISegment(Vector3 begin, Vector3 end)
		{
			Begin = begin;
			End = end;
			Length = begin.DistanceTo(end);
		}
	}
	
	private Array<GSAISegment> _segments = new Array<GSAISegment>();
	private Vector3 _nearestPointOnSegment;
	private Vector3 _nearestPointOnPath;
	
	/**
	 * @brief If false, the path loops.
	 */
	public bool IsOpen { get; set; }
	public float Length { get; set; }
	
	public GSAIPath(Array<Vector3> waypoints, bool isOpen = false)
	{
		IsOpen = isOpen;
		CreatePath(waypoints);
		_nearestPointOnSegment = waypoints[0];
		_nearestPointOnPath = waypoints[0];
	}
	
	/**
	 * @brief Creates a path from a list of waypoints.
	 */
	public void CreatePath(Array<Vector3> waypoints)
	{
		if (waypoints.Count < 2)
		{
			GD.PushError("Waypoints must contain at least two waypoints.");
			return;
		}
		
		_segments.Clear();
		Length = 0;
		
		Vector3 current = waypoints[0];
		Vector3 previous = Vector3.Zero;
		
		for (int i = 1; i <= waypoints.Count; i++)
		{
			previous = current;
			
			if (i < waypoints.Count)
			{
				current = waypoints[i];
			}
			else if (IsOpen)
			{
				break;
			}
			else
			{
				current = waypoints[0];
			}
			
			GSAISegment segment = new GSAISegment(previous, current);
			Length += segment.Length;
			segment.CumulativeLength = Length;
			_segments.Add(segment);
		}
	}
	
	/**
	 * @return Distance from `agentCurrentPosition` to the next waypoint.
	 */
	public float CalculateDistance(Vector3 agentCurrentPosition)
	{
		if (_segments.Count == 0)
		{
			return 0.0f;
		}
		
		float smallestDistanceSquared = Mathf.Inf;
		GSAISegment nearestSegment = null;
		
		foreach (GSAISegment segment in _segments)
		{
			float distanceSquared = calculatePointSegmentDistanceSquared(
				segment.Begin, segment.End, agentCurrentPosition
			);
			
			if (distanceSquared < smallestDistanceSquared)
			{
				_nearestPointOnPath = _nearestPointOnSegment;
				smallestDistanceSquared = distanceSquared;
				nearestSegment = segment;
			}
		}
		
		return nearestSegment.CumulativeLength
			- _nearestPointOnPath.DistanceTo(nearestSegment.End);
	}
	
	/**
	 * @brief Calculates target position from the path's starting point based on
	 *        `targetDistance`.
	 */
	public Vector3 CalculateTargetPosition(float targetDistance)
	{
		if (IsOpen)
		{
			targetDistance = Mathf.Clamp(targetDistance, 0.0f, Length);
		}
		else if (targetDistance < 0.0f)
		{
			targetDistance = Length + targetDistance % Length;
		}
		else if (targetDistance > Length)
		{
			targetDistance = targetDistance % Length;
		}
		
		GSAISegment desiredSegment = null;
		foreach (GSAISegment segment in _segments)
		{
			if (segment.CumulativeLength >= targetDistance)
			{
				desiredSegment = segment;
				break;
			}
		}
		
		if (desiredSegment == null)
		{
			desiredSegment = _segments[-1];
		}
		
		float distance = desiredSegment.CumulativeLength - targetDistance;
		Vector3 segmentLength = desiredSegment.Begin - desiredSegment.End;
		float segmentCount = distance / desiredSegment.Length;
		
		return segmentLength * segmentCount + desiredSegment.End;
	}
	
	/**
	 * @return Position of the first point on the path.
	 */
	public Vector3 GetStartPoint()
	{
		return _segments[0].Begin;
	}
	
	/**
	 * @return Position of the very last point on the path.
	 */
	public Vector3 GetEndPoint()
	{
		return _segments[-1].End;
	}
	
	private float calculatePointSegmentDistanceSquared(
		Vector3 start,
		Vector3 end,
		Vector3 position)
	{
		_nearestPointOnSegment = start;
		Vector3 startEnd = end - start;
		float startEndLengthSquared = startEnd.LengthSquared();
		
		if (startEndLengthSquared != 0.0f)
		{
			float t = (position - start).Dot(startEnd) / startEndLengthSquared;
			_nearestPointOnSegment += startEnd * Mathf.Clamp(t, 0.0f, 1.0f);
		}
		
		return _nearestPointOnSegment.DistanceSquaredTo(position);
	}
}

using System;
using Godot;

/**
 * @brief Base type for group-based steering behaviors.
 */
public class GSAIGroupBehavior : GSAISteeringBehavior
{
	protected Func<GSAISteeringAgent, bool> _callback;
	
	/**
	 * @brief Group of agents in which the group behavior is calculated.
	 */
	public GSAIProximity Proximity { get; set; }
	
	public GSAIGroupBehavior(
		GSAISteeringAgent agent,
		GSAIProximity proximity
	) : base(agent)
	{
		Proximity = proximity;
		_callback = reportNeighbor;
	}
	
	/**
	 * @brief Internal callback used to decide whether a member is relevant.
	 */
	protected virtual bool reportNeighbor(GSAISteeringAgent neighbor)
	{
		return false;
	}
}

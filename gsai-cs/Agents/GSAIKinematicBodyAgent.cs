using System;
using Godot;

/**
 * @brief Base class for kinematic body agents.
 */
public class GSAIKinematicBodyAgent : GSAISpecializedAgent
{
	/**
	 * SLIDE uses `MoveAndSlide`
	 * COLLIDE uses `MoveAndCollide`
	 * POSITION changes `GlobalPosition` directly
	 */
	public enum MovementTypes { SLIDE, COLLIDE, POSITION }
	
	/**
	 * @brief Type of movement the body executes.
	 */
	public MovementTypes MovementType { get; set; }
	
	public override void ApplySteering(
		GSAITargetAcceleration acceleration,
		float delta)
	{
		_appliedSteering = true;
		
		switch (MovementType)
		{
			case MovementTypes.SLIDE:
				applySlidingSteering(acceleration.Linear, delta);
				break;
			case MovementTypes.COLLIDE:
				applyCollideSteering(acceleration.Linear, delta);
				break;
			case MovementTypes.POSITION:
				applyPositionSteering(acceleration.Linear, delta);
				break;
			default:
				GD.PushError("GSAIKinematicBodyAgent: Unhandled switch case");
				break;
		}
		
		applyOrientationSteering(acceleration.Angular, delta);
	}
	
	protected virtual void applySlidingSteering(
		Vector3 acceleration,
		float delta)
	{
		return;
	}
	
	protected virtual void applyCollideSteering(
		Vector3 acceleration,
		float delta)
	{
		return;
	}
	
	protected virtual void applyPositionSteering(
		Vector3 acceleration,
		float delta)
	{
		return;
	}
	
	protected virtual void applyOrientationSteering(
		float acceleration,
		float delta)
	{
		return;
	}
	
	protected Vector3 adjustLinearVelocity(Vector3 velocity)
	{
		velocity = GSAIUtils.ClampedV3(velocity, LinearSpeedMax);
		
		if (ApplyLinearDrag)
		{
			velocity = velocity.LinearInterpolate(
				Vector3.Zero, LinearDragPercentage);
		}
		
		return velocity;
	}
	
	protected float adjustAngularVelocity(float velocity)
	{
		velocity = Mathf.Clamp(
			velocity, -AngularAccelerationMax, AngularAccelerationMax
		);
		
		if (ApplyAngularDrag)
		{
			velocity = Mathf.Lerp(velocity, 0.0f, AngularDragPercentage);
		}
		
		return velocity;
	}
}

class_name GSAIPriority
extends GSAISteeringBehavior

var last_selected_index: int setget _set_last_selected_index, \
		_get_last_selected_index
var zero_threshold: float setget _set_zero_threshold, _get_zero_threshold

func _init(_agent: GSAISteeringAgent = null, \
		_zero_threshold: float = 0.001) -> void:
	if _agent == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIPriority").new(_agent._cs, _zero_threshold)

func add(behavior: GSAISteeringBehavior) -> void:
	_cs.Add(behavior._cs)

func get_behavior_at(index: int) -> GSAISteeringBehavior:
	return _get_wrapper_from_cs(_cs.GetBehaviorAt(index)) \
			as GSAISteeringBehavior

func _set_last_selected_index(new: int) -> void:
	_cs.LastSelectedIndex = new

func _get_last_selected_index() -> int:
	return _cs.LastSelectedIndex

func _set_zero_threshold(new: float) -> void:
	_cs.ZeroThreshold = new

func _get_zero_threshold() -> float:
	return _cs.ZeroThreshold

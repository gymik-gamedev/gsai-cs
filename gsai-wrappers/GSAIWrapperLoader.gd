class_name GSAIWrapperLoader
extends Object
# Used to load GSAI wrappers without introducing cyclic dependencies

static func get_wrapper_by_name(name: String) -> Object:
	var wrapper: Object = null
	
	match name:
		"GSAIAgentLocation":
			wrapper = GSAIAgentLocation.new()
		"GSAIArrive":
			wrapper = GSAIArrive.new()
		"GSAIAvoidCollisions":
			wrapper = GSAIAvoidCollisions.new()
		"GSAIBlend":
			wrapper = GSAIBlend.new()
		"GSAICohesion":
			wrapper = GSAICohesion.new()
		"GSAIEvade":
			wrapper = GSAIEvade.new()
		"GSAIFace":
			wrapper = GSAIFace.new()
		"GSAIFlee":
			wrapper = GSAIFlee.new()
		"GSAIFollowPath":
			wrapper = GSAIFollowPath.new()
		"GSAIGroupBehavior":
			wrapper = GSAIGroupBehavior.new()
		"GSAIInfiniteProximity":
			wrapper = GSAIInfiniteProximity.new()
		"GSAIKinematicBody2DAgent":
			wrapper = GSAIKinematicBody2DAgent.new()
		"GSAIKinematicBody3DAgent":
			wrapper = GSAIKinematicBody3DAgent.new()
		"GSAILookWhereYouGo":
			wrapper = GSAILookWhereYouGo.new()
		"GSAIMatchOrientation":
			wrapper = GSAIMatchOrientation.new()
		"GSAIPath":
			wrapper = GSAIPath.new()
		"GSAIPriority":
			wrapper = GSAIPriority.new()
		"GSAIProximity":
			wrapper = GSAIProximity.new()
		"GSAIPursue":
			wrapper = GSAIPursue.new()
		"GSAIRadiusProximity":
			wrapper = GSAIRadiusProximity.new()
		"GSAIRigidBody2DAgent":
			wrapper = GSAIRigidBody2DAgent.new()
		"GSAIRigidBody3DAgent":
			wrapper = GSAIRigidBody3DAgent.new()
		"GSAISeek":
			wrapper = GSAISeek.new()
		"GSAISeparation":
			wrapper = GSAISeparation.new()
		"GSAISpecializedAgent":
			wrapper = GSAISpecializedAgent.new()
		"GSAISteeringAgent":
			wrapper = GSAISteeringAgent.new()
		"GSAISteeringBehavior":
			wrapper = GSAISteeringBehavior.new()
		"GSAITargetAcceleration":
			wrapper = GSAITargetAcceleration.new()
		_:
			push_error("GSAIWrapperLoader: No known wrapper for given C# class")
			return null
	
	return wrapper

class_name GSAISeparation
extends GSAIGroupBehavior

var decay_coefficient: float setget _set_decay_coefficient, \
		_get_decay_coefficient

func _init(_agent: GSAISteeringAgent = null, \
		_proximity: GSAIProximity = null) -> void:
	if _agent == null or _proximity == null:
		return
	
	_cs = _get_cs("Behaviors/GSAISeparation").new(_agent._cs, _proximity._cs)

func _set_decay_coefficient(new: float) -> void:
	_cs.DecayCoefficient = new

func _get_decay_coefficient() -> float:
	return _cs.DecayCoefficient

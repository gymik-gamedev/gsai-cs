class_name GSAIEvade
extends GSAIPursue

func _init(_agent: GSAISteeringAgent = null, _target: GSAISteeringAgent \
		= null, predict_time_max: float = 1.0) -> void:
	if _agent == null or _target == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIEvade").new(_agent._cs, _target._cs, \
			predict_time_max)

class_name GSAIFace
extends GSAIMatchOrientation

func _init(_agent: GSAISteeringAgent = null, _target: GSAIAgentLocation \
		= null, use_z: bool = false) -> void:
	if _agent == null or _target == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIFace").new(_agent._cs, _target._cs, use_z)

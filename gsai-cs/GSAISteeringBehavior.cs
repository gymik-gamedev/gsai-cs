using System;
using Godot;

/**
 * @brief Base class for all steering behaviors.
 *
 * Steering behaviors calculate the linear and the angular acceleration to be
 * to the agent that owns them.
 *
 * @note The `CalculateSteering` function is the entry point for all behaviors.
 *       Individual steering behaviors encapsulate the steering logic.
 */
public class GSAISteeringBehavior : Godot.Object
{
	public bool IsEnabled { get; set; } = true;
	public GSAISteeringAgent Agent { get; set; }
	
	public GSAISteeringBehavior(GSAISteeringAgent agent)
	{
		Agent = agent;
	}
	
	/**
	 * @brief Sets the `acceleration` with the behavior's desired amount of
	 *        acceleration.
	 */
	public void CalculateSteering(GSAITargetAcceleration acceleration)
	{
		if (IsEnabled)
		{
			calculateSteering(acceleration);
		}
		else
		{
			acceleration.SetZero();
		}
	}
	
	protected virtual void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		acceleration.SetZero();
	}
}

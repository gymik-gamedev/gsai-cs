class_name GSAIKinematicBody3DAgent
extends GSAISpecializedAgent

enum MovementType { SLIDE, COLLIDE, POSITION }

var body: KinematicBody setget _set_body, _get_body
var movement_type: int setget _set_movement_type, _get_movement_type

func _init(_body: KinematicBody = null, \
		_movement_type: int = MovementType.SLIDE) -> void:
	if _body == null:
		return
	
	_cs = _get_cs("Agents/GSAIKinematicBody3DAgent").new(_body, _movement_type)

func _set_body(new: KinematicBody) -> void:
	_cs.Body = new

func _get_body() -> KinematicBody:
	return _cs.Body

func _set_movement_type(new: int) -> void:
	_cs.MovementType = new

func _get_movement_type() -> int:
	return _cs.MovementType

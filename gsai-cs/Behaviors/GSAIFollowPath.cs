using System;
using Godot;

/**
 * @brief Produces linear acceleration that moves the agent along a path.
 */
public class GSAIFollowPath : GSAIArrive
{
	public GSAIPath Path { get; set; }
	
	/**
	 * @brief Whether to use `GSAIArrive`behavior on an open path.
	 */
	public bool IsArriveEnabled { get; set; } = true;
	
	/**
	 * @brief The distance along the path to generate the next target position.
	 */
	public float PathOffset { get; set; }
	
	/**
	 * @brief Amount of time in the future to predict the agent's position.
	 * @note Setting this to 0.0 will force non-predictive path following.
	 */
	public float PredictionTime { get; set; }
	
	public GSAIFollowPath(
		GSAISteeringAgent agent,
		GSAIPath path,
		float pathOffset = 0.0f,
		float predictionTime = 0.0f
	) : base(agent, null)
	{
		Path = path;
		PathOffset = pathOffset;
		PredictionTime = predictionTime;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		Vector3 location = PredictionTime == 0.0f ?
			Agent.Position :
			Agent.Position + (Agent.LinearVelocity * PredictionTime);
		
		float distance = Path.CalculateDistance(location);
		float targetDistance = distance + PathOffset;
		
		if (PredictionTime > 0.0f && Path.IsOpen
			&& targetDistance < Path.CalculateDistance(Agent.Position))
		{
			targetDistance = Path.Length;
		}
		
		Vector3 targetPosition = Path.CalculateTargetPosition(targetDistance);
		
		if (IsArriveEnabled && Path.IsOpen && (
			PathOffset >= 0.0f
			&& targetDistance > Path.Length - DecelerationRadius
			|| PathOffset < 0.0f && targetDistance < DecelerationRadius
			)
		)
		{
			arrive(acceleration, targetPosition);
			return;
		}
		
		acceleration.Linear = Agent.Position.DirectionTo(targetPosition);
		acceleration.Linear *= Agent.LinearAccelerationMax;
		acceleration.Angular = 0.0f;
	}
}

class_name GSAIProximity
extends GSAIWrapper

var agent: GSAISteeringAgent setget _set_agent, _get_agent
var agents: Array setget _set_agents, _get_agents

func _init(_agent: GSAISteeringAgent = null, _agents: Array = []) -> void:
	if _agent == null:
		return
	
	var agents_cs: Array = []
	for agent in _agents:
		agents_cs.append(agent._cs)
	
	_cs = _get_cs("Proximities/GSAIProximity").new(_agent._cs, \
			agents_cs)

func _set_agent(new: GSAISteeringAgent) -> void:
	if new == null:
		return
	
	_cs.Agent = new._cs

func _get_agent() -> GSAISteeringAgent:
	return _get_wrapper_from_cs(_cs.Agent) as GSAISteeringAgent

func _set_agents(new: Array) -> void:
	var agents_cs: Array = []
	
	for agent in new:
		agents_cs.append(agent._cs)
	
	_cs.Agents = agents_cs

func _get_agents() -> Array:
	var _agents: Array = []
	
	for i in range(_cs.Agents.size()):
		_agents.append(_get_wrapper_from_cs(_cs.Agents[i]))
	
	return _agents

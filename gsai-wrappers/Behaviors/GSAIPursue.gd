class_name GSAIPursue
extends GSAISteeringBehavior

var target: GSAISteeringAgent setget _set_target, _get_target
var predict_time_max: float setget _set_predict_time_max, _get_predict_time_max

func _init(_agent: GSAISteeringAgent = null, _target: GSAISteeringAgent \
		= null, _predict_time_max: float = 1.0) -> void:
	if _agent == null or _target == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIPursue").new(_agent._cs, _target._cs, \
			_predict_time_max)

func _set_target(new: GSAISteeringAgent) -> void:
	_cs.Target = new._cs

func _get_target() -> GSAISteeringAgent:
	return _get_wrapper_from_cs(_cs.Target) as GSAISteeringAgent

func _set_predict_time_max(new: float) -> void:
	_cs.PredictTimeMax = new

func _get_predict_time_max() -> float:
	return _cs.PredictTimeMax

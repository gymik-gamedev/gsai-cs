class_name GSAIPath
extends GSAIWrapper

var is_open: bool setget _set_is_open, _get_is_open
var length: float setget _set_length, _get_length

func _init(waypoints: Array = [], _is_open: bool = false) -> void:
	if waypoints.empty():
		return
	
	_cs = _get_cs("GSAIPath").new(waypoints, _is_open)

func create_path(waypoints: Array) -> void:
	_cs.CreatePath(waypoints)

func calculate_distance(agent_current_position: Vector3) -> float:
	return _cs.CalculateDistance(agent_current_position)

func calculate_target_position(target_distance: float) -> Vector3:
	return _cs.CalculateTargetPosition(target_distance)

func get_start_point() -> Vector3:
	return _cs.GetStartPoint()

func get_end_point() -> Vector3:
	return _cs.GetEndPoint()

func _set_is_open(new: bool) -> void:
	_cs.IsOpen = new

func _get_is_open() -> bool:
	return _cs.IsOpen

func _set_length(new: float) -> void:
	_cs.Length = new

func _get_length() -> float:
	return _cs.Length

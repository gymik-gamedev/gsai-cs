class_name GSAIAvoidCollisions
extends GSAIGroupBehavior

func _init(_agent: GSAISteeringAgent = null, \
		_proximity: GSAIProximity = null) -> void:
	if _agent == null or _proximity == null:
		return
	
	_cs = _get_cs("Behaviors/GSAIAvoidCollisions").new(_agent._cs, \
			_proximity._cs)

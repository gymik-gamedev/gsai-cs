class_name GSAISteeringAgent
extends GSAIAgentLocation

var zero_linear_speed_threshold: float setget \
		_set_zero_linear_speed_threshold, _get_zero_linear_speed_threshold
var linear_speed_max: float setget _set_linear_speed_max, _get_linear_speed_max
var linear_acceleration_max: float setget _set_linear_accel_max, \
		_get_linear_accel_max
var angular_speed_max: float setget _set_angular_speed_max, \
		_get_angular_speed_max
var angular_acceleration_max: float setget _set_angular_accel_max, \
		_get_angular_accel_max
var linear_velocity: Vector3 setget _set_linear_velocity, _get_linear_velocity
var angular_velocity: float setget _set_angular_velocity, _get_angular_velocity
var bounding_radius: float setget _set_bounding_radius, _get_bounding_radius
var is_tagged: bool setget _set_is_tagged, _get_is_tagged

func _init() -> void:
	_cs = _get_cs("GSAISteeringAgent").new()

func _set_zero_linear_speed_threshold(new: float) -> void:
	_cs.ZeroLinearSpeedThreshold = new

func _get_zero_linear_speed_threshold() -> float:
	return _cs.ZeroLinearSpeedThreshold

func _set_linear_speed_max(new: float) -> void:
	_cs.LinearSpeedMax = new

func _get_linear_speed_max() -> float:
	return _cs.LinearSpeedMax

func _set_linear_accel_max(new: float) -> void:
	_cs.LinearAccelerationMax = new

func _get_linear_accel_max() -> float:
	return _cs.LinearAccelerationMax

func _set_angular_speed_max(new: float) -> void:
	_cs.AngularSpeedMax = new

func _get_angular_speed_max() -> float:
	return _cs.AngularSpeedMax

func _set_angular_accel_max(new: float) -> void:
	_cs.AngularAccelerationMax = new

func _get_angular_accel_max() -> float:
	return _cs.AngularAccelerationMax

func _set_linear_velocity(new: Vector3) -> void:
	_cs.LinearVelocity = new

func _get_linear_velocity() -> Vector3:
	return _cs.LinearVelocity

func _set_angular_velocity(new: float) -> void:
	_cs.AngularVelocity = new

func _get_angular_velocity() -> float:
	return _cs.AngularVelocity

func _set_bounding_radius(new: float) -> void:
	_cs.BoundingRadius = new

func _get_bounding_radius() -> float:
	return _cs.BoundingRadius

func _set_is_tagged(new: bool) -> void:
	_cs.IsTagged = new

func _get_is_tagged() -> bool:
	return _cs.IsTagged

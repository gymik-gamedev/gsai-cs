class_name GSAITargetAcceleration
extends GSAIWrapper

var linear: Vector3 setget _set_linear, _get_linear
var angular: float setget _set_angular, _get_angular

func _init() -> void:
	_cs = _get_cs("GSAITargetAcceleration").new()

func set_zero() -> void:
	_cs.SetZero()

func add_scaled_accel(accel: GSAITargetAcceleration, scalar: float) -> void:
	_cs.AddScaledAccel(accel._cs, scalar)

func get_magnitude_squared() -> float:
	return _cs.GetMagnitudeSquared()

func get_magnitude() -> float:
	return _cs.GetMagnitude()

func _set_linear(new: Vector3) -> void:
	_cs.Linear = new

func _get_linear() -> Vector3:
	return _cs.Linear

func _set_angular(new: float) -> void:
	_cs.Angular = new

func _get_angular() -> float:
	return _cs.Angular

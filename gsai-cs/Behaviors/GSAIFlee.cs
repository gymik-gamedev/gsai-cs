using System;
using Godot;

/**
 * @brief Calculates acceleration to take agent directly away from the target.
 */
public class GSAIFlee : GSAISeek
{
	public GSAIFlee(
		GSAISteeringAgent agent,
		GSAIAgentLocation target
	) : base(agent, target)
	{
		return;
	}
	
	protected override void calculateSteering(
		GSAITargetAcceleration acceleration)
	{
		Vector3 dirFromTarget = Agent.Position.DirectionTo(Target.Position);
		acceleration.Linear = dirFromTarget * Agent.LinearAccelerationMax;
		acceleration.Angular = 0.0f;
	}
}

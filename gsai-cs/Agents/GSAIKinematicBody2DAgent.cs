using System;
using Godot;

/**
 * @brief Specialized steering agent based on KinematicBody2D that auto-updates
 *        itself every frame.
 */
public class GSAIKinematicBody2DAgent : GSAIKinematicBodyAgent
{
	private KinematicBody2D _body;
	private Vector2 _lastPosition;
	
	/**
	 * @brief The KinematicBody2D to keep track of.
	 */
	public KinematicBody2D Body
	{
		get { return _body; }
		set
		{
			_body = value;
			
			_lastPosition = value.GlobalPosition;
			_lastOrientation = value.Rotation;
			
			Position = GSAIUtils.ToVector3(_lastPosition);
			Orientation = _lastOrientation;
		}
	}
	
	/**
	 * @warning Body and MovementType are set only after the body gets inside
	 *          tree!
	 */
	public GSAIKinematicBody2DAgent(
		KinematicBody2D body,
		MovementTypes movementType = MovementTypes.SLIDE)
	{
		if (!body.IsInsideTree())
		{
			Godot.Collections.Array args = new Godot.Collections.Array();
			args.Add(body);
			args.Add(movementType);
			body.Connect("ready", this, nameof(onBodyReady), args);
		}
		else
		{
			onBodyReady(body, movementType);
		}
	}
	
	/**
	 * @brief Updates position, orientation, and velocities.
	 * @note Triggered automatically each frame by Godot.
	 */
	public void OnSceneTreePhysicsFrame()
	{
		if (Body == null)
		{
			return;
		}
		
		Vector2 currentPosition = Body.GlobalPosition;
		float currentOrientation = Body.Rotation;
		
		Position = GSAIUtils.ToVector3(currentPosition);
		Orientation = currentOrientation;
		
		if (!CalculateVelocities)
		{
			return;
		}
		else if (_appliedSteering)
		{
			_appliedSteering = false;
			return;
		}
		
		LinearVelocity = adjustLinearVelocity(
			GSAIUtils.ToVector3(currentPosition - _lastPosition)
		);
		
		AngularVelocity = adjustAngularVelocity(
			_lastOrientation - currentOrientation
		);
		
		_lastPosition = currentPosition;
		_lastOrientation = currentOrientation;
	}
	
	protected override void applySlidingSteering(
		Vector3 acceleration,
		float delta)
	{
		if (Body == null)
		{
			return;
		}
		
		Vector3 velocity = adjustLinearVelocity(
			LinearVelocity + acceleration * delta
		);
		velocity = GSAIUtils.ToVector3(
			Body.MoveAndSlide(GSAIUtils.ToVector2(velocity))
		);
		
		if (CalculateVelocities)
		{
			LinearVelocity = velocity;
		}
	}
	
	protected override void applyCollideSteering(
		Vector3 acceleration,
		float delta)
	{
		if (Body == null)
		{
			return;
		}
		
		Vector3 velocity = adjustLinearVelocity(
			LinearVelocity + acceleration * delta
		);
		Body.MoveAndCollide(GSAIUtils.ToVector2(velocity) * delta);
		
		if (CalculateVelocities)
		{
			LinearVelocity = velocity;
		}
	}
	
	protected override void applyPositionSteering(
		Vector3 acceleration,
		float delta)
	{
		if (Body == null)
		{
			return;
		}
		
		Vector3 velocity = adjustLinearVelocity(
			LinearVelocity + acceleration * delta
		);
		Body.GlobalPosition += GSAIUtils.ToVector2(velocity) * delta;
		
		if (CalculateVelocities)
		{
			LinearVelocity = velocity;
		}
	}
	
	protected override void applyOrientationSteering(
		float acceleration,
		float delta)
	{
		if (Body == null)
		{
			return;
		}
		
		float velocity = adjustAngularVelocity(
			AngularVelocity + acceleration * delta
		);
		Body.Rotation += velocity * delta;
		
		if (CalculateVelocities)
		{
			AngularVelocity = velocity;
		}
	}
	
	private void onBodyReady(KinematicBody2D body, MovementTypes movementType)
	{
		MovementType = movementType;
		Body = body;
		
		Body.GetTree().Connect(
			"physics_frame",
			this,
			nameof(OnSceneTreePhysicsFrame));
	}
}

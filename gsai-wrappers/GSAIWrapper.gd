class_name GSAIWrapper
extends Reference
# Base class for GDScript wrappers

const CSHARP_DIR_PATH: String = "res://libraries/gsai-cs"
const THIS_DIR_PATH: String = "res://libraries/gsai-wrappers"

var _cs: Object = null
var _context: Object = load(THIS_DIR_PATH + "/GSAICSharpContext.cs")
var _loader: Object = load(THIS_DIR_PATH + "/GSAIWrapperLoader.gd")

func _get_cs(relative_path: String) -> CSharpScript:
	var absolute_path: String = CSHARP_DIR_PATH + "/" + relative_path + ".cs"
	
	if ResourceLoader.exists(absolute_path):
		return ResourceLoader.load(absolute_path) as CSharpScript
	else:
		push_error("Script not found: " + absolute_path + ".")
		return null

func _get_wrapper_from_cs(cs: Object) -> GSAIWrapper:
	var wrapper_name: String = _context.GetClassName(cs)
	var wrapper: GSAIWrapper = _loader.get_wrapper_by_name(wrapper_name)
	wrapper._cs = cs
	return wrapper
